import 'package:ecommerce_app/pages/auth/notification/notification.dart';
import 'package:flutter/material.dart';

import 'filters_screen/filters_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int index = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         title: const Text(
            "Universe",
          style: TextStyle(
            fontSize: 24,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.notifications),
             onPressed: () {
               Navigator.push(
                   context,
                   MaterialPageRoute(
                       builder: (context) => const NotificationScreen()));

             },
          ), //IconButton
          IconButton(
            icon: const Icon(Icons.shopping_cart),
             onPressed: () {},
          ), //IconButton
        ], //<Widget>[]
         elevation: 50.0,
        leading: IconButton(
          icon: const Icon(Icons.menu),
           onPressed: () {},
        ),
       ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: 18.0,
              vertical: 34.0
          ),
          child: Column(
            children: [
              Row(
                  children: [
                   Expanded(
                    child: Container(
                      height: 42,
                      color: const Color( 0xffF3F3F5),
                      child: TextFormField(
                        decoration:  const InputDecoration(
                          enabledBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: 'Search',
                          hintStyle: TextStyle(
                            color: Color(0xff707070),
                          ),
                          prefixIcon: Icon(
                              Icons.search,
                            color: Color( 0xff707070),
                            ),
                          ),
                         ),
                      ),
                    ),
                  const SizedBox(width: 12,),
                  Container(
                    height: 42,
                    decoration: const BoxDecoration(
                      color: Color( 0xffF3F3F5),
                     ),
                    child: IconButton(
                      onPressed: (){
                        Navigator.push(
                        context,
                        MaterialPageRoute(
                        builder: (context) => const FiltersScreen()));
                        },
                      icon: const Icon(Icons.filter_alt_sharp),
                    ),
                   ),
                ],
              ),
              const SizedBox(height: 32,),
              Row(
                children: [
                const Text(
                'Resently added',
                style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                ),
                ),
                 const Expanded(
                   child: Divider(
                      indent: 9,
                     endIndent: 9,
                    ),
                 ),
                 TextButton(
                onPressed: (){},
                  child:const Text(
                    'See all',
                    style: TextStyle(
                      color:Color(0xff003366),
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30.0),
                child: SizedBox(
                  height: 180,
                  child: GridView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 10,
                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 180,
                      mainAxisSpacing: 25,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return const Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: GridBuilder(),
                      );
                    },
                  ),
                ),
              ),
              Row(
                children: [
                const Text(
                'Resently added',
                style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                ),
                ),
                 const Expanded(
                   child: Divider(
                      indent: 9,
                     endIndent: 9,
                    ),
                 ),
                 TextButton(
                onPressed: (){},
                  child:const Text(
                    'See all',
                    style: TextStyle(
                      color:Color(0xff003366),
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 21.0),
                child: SizedBox(
                  height: 40,
                  child: GridView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 10,
                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 40,
                      mainAxisSpacing: 15,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return  const Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: CatGridBuilder(),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 180,
                child: GridView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 10,
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 180,
                    mainAxisSpacing: 25,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    return const Padding(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: GridBuilder(),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
         decoration:const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color(0xff003366),
              Color(0xff003366),
              Color(0x802c6dad),
            ],
          ),
        ),
        child: BottomNavigationBar(
            items: const [
              BottomNavigationBarItem(
                icon: Icon(
                    Icons.settings,
                  size: 24,
                  color: Colors.white,
                ),
                label:  " ",
                activeIcon: Icon(
                  Icons.settings,
                  size: 34,
                  color: Colors.white,
                ),
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home_filled,
                  size: 24,
                  color: Colors.white,
                ),
                  activeIcon:Icon(
                    Icons.home_filled,
                    size: 34,
                    color: Colors.white,
                  ) ,
                  label:  " ",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person,
                  size: 24,
                  color: Colors.white,
                ),
                activeIcon: Icon(Icons.person,
                  size: 34,
                  color: Colors.white,
                ),
                label:  " ",
              ),
            ],
          currentIndex: index,
          onTap: (int i){setState((){index = i;});},
          fixedColor: Colors.white,
          backgroundColor: Colors.transparent,
        ),
      ),
    );
  }
}

class GridBuilder extends StatelessWidget {
  const GridBuilder({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
       decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
           boxShadow: kElevationToShadow[4],
      ),
       child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 6.0),
            child: Container(
              width: 158,
              height: 85,
              decoration:   BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: const DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage('https://i.pinimg.com/originals/c9/4b/e5/c94be57f6fcd763e9782985b85559125.jpg'),
                ),
              ),
            ),
          ),
           const Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0,),
            child: Center(
              child: Text('Panka Chair'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14.0),
            child: Row(
              children: [
                const Text(
                    '\$1000',
                  style: TextStyle(
                    fontSize: 14,
                    color:Color( 0xff798CAA)
                  ),
                ),
                const Spacer(),
                CircleAvatar(
                  radius: 14,
                  backgroundColor: const Color( 0xffF3F3F5),
                  child: IconButton(
                    icon: const Icon(
                        Icons.shopping_cart,
                      size: 14,
                      color: Color(0xff003366),
                    ),
                    onPressed: () {},
                  ),
                ), //IconButton

              ],
            ),
          ),
        ],
      ),
    );
  }
}
class CatGridBuilder extends StatelessWidget {
  const CatGridBuilder({super.key});


  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(20),
      focusColor:  const Color(0xff003366),
      hoverColor:const Color(0xffF2F2F4),
      onTap:() {
    },
      child: Container(
          decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
            color:const Color(0xffF2F2F4) ,
      ),
        child: const Center(
          child: Text(
            "Shirt",
              style: TextStyle(
                fontSize: 16,
               ),
                    ),
        ),
      ),
    );
  }
}
