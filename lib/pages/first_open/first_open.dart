import 'package:ecommerce_app/pages/first_open/models/first_open_model.dart';
import 'package:flutter/material.dart';

import '../../core/constants/theme.dart';

class FirstOpenPage extends StatefulWidget {
  const FirstOpenPage({super.key});

  @override
  State<FirstOpenPage> createState() => _FirstOpenPageState();
}

class _FirstOpenPageState extends State<FirstOpenPage> {
  int currentIndex = 0;
  late PageController _pageController;
  int index1 = 0;
  List<FirstOpenModel> screens = <FirstOpenModel>[
    FirstOpenModel(
      img: 'assets/images/Online_shopping.png',
      text: "Your Shopping World at Your Fingertips",
      desc:
          "Discover \"MarketVerse,\" the app that brings you the best products from around the globe. With its user-friendly and intuitive design, you can easily browse and purchase your favorite items securely and effortlessly.",
      bg: Colors.white,
      button: Color(0xFF4756DF),
    ),
    FirstOpenModel(
      img: 'assets/images/shopping_cart.png',
      text: "Shop Without Limits with MarketVerse",
      desc:
          "MarketVerse\" is your gateway to the world of online shopping where choices are endless. From trendy fashion to smart electronics, everything you need and more is readily available with ease.",
      bg: Color(0xFF4756DF),
      button: Colors.white,
    ),
    FirstOpenModel(
      img: 'assets/images/online_store.png',
      text: "An Unmatched Shopping Experience with MarketVerse",
      desc:
          "A unique shopping adventure awaits you with \"MarketVerse,\" where we offer an endless variety of products, exclusive deals, and competitive pricing. All in one app that guarantees a convenient and enjoyable shopping experience.",
      bg: Colors.white,
      button: Color(0xFF4756DF),
    ),
  ];

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: ThemeColors.primary,
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                currentIndex != 0
                    ? TextButton(
                        onPressed: () {
                          currentIndex--;
                          _pageController.previousPage(
                            duration: Duration(milliseconds: 300),
                            curve: Curves.bounceIn,
                          );
                          setState(() {});
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.arrow_back_ios,
                              color: Colors.white,
                              size: 15,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "Back",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            )
                          ],
                        ))
                    : SizedBox(),
                TextButton(
                    onPressed: () {
                      //TODO: navigate to next screen
                    },
                    child: Row(
                      children: [
                        Text(
                          "Skip",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.arrow_forward,
                          color: Colors.white,
                          size: 20,
                        )
                      ],
                    ))
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                        child: PageView.builder(
                            itemCount: screens.length,
                            controller: _pageController,
                            physics: NeverScrollableScrollPhysics(),
                            onPageChanged: (int index) {
                              setState(() {
                                currentIndex = index;
                              });
                            },
                            itemBuilder: (_, index) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Image.asset(
                                    screens[index].img,
                                    width: size.width * 0.8,
                                    fit: BoxFit.fitWidth,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Text(
                                    screens[index].text,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Roboto',
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    width: 260,
                                    child: Text(
                                      screens[index].desc,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontFamily: 'Roboto',
                                        color: Colors.white.withOpacity(0.8),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            }),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 10.0,
                          margin: EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: ListView.builder(
                            itemCount: screens.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 3.0),
                                      width: 22,
                                      height: 6,
                                      decoration: BoxDecoration(
                                        color: currentIndex == index
                                            ? Color(0XFFFFA07A)
                                            : Color(0xFFD3D3D3),
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                    ),
                                  ]);
                            },
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            if (currentIndex == screens.length - 1) {
                              //TODO: Navigate to next screen
                              return; //remove it when add navigation
                            }
                            setState(() {
                              currentIndex++;
                            });
                            _pageController.nextPage(
                              duration: Duration(milliseconds: 300),
                              curve: Curves.bounceIn,
                            );
                          },
                          child: Container(
                            height: 75,
                            width: 75,
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: Colors.white, width: 2)),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: Icon(
                                Icons.arrow_forward,
                                color: ThemeColors.primary,
                                size: 40,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
            // Container(
            //   height: size.height * 0.15,
            //   child: Column(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: [
            //       Container(
            //         height: 10.0,
            //         margin: EdgeInsets.only(top: 20),
            //         alignment: Alignment.center,
            //         child: ListView.builder(
            //           itemCount: screens.length,
            //           shrinkWrap: true,
            //           scrollDirection: Axis.horizontal,
            //           itemBuilder: (context, index) {
            //             return Row(
            //                 mainAxisAlignment: MainAxisAlignment.center,
            //                 children: [
            //                   Container(
            //                     margin: EdgeInsets.symmetric(horizontal: 3.0),
            //                     width: currentIndex == index ? 25 : 8,
            //                     height: 8,
            //                     decoration: BoxDecoration(
            //                       color: currentIndex == index
            //                           ? Colors.red
            //                           : Color(0xFF963B7B),
            //                       borderRadius: BorderRadius.circular(10.0),
            //                     ),
            //                   ),
            //                 ]);
            //           },
            //         ),
            //       ),
            //       Container(
            //         alignment: Alignment.bottomRight,
            //         child: TextButton(
            //           onPressed: () async {
            //             if (currentIndex == screens.length - 1) {
            //               //TODO: Navigate to next screen
            //             }
            //             setState(() {
            //               currentIndex++;
            //             });
            //             _pageController.nextPage(
            //               duration: Duration(milliseconds: 300),
            //               curve: Curves.bounceIn,
            //             );
            //           },
            //           child: Row(
            //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               mainAxisSize: MainAxisSize.min,
            //               children: [
            //                 Text(
            //                   "Next",
            //                   style: TextStyle(
            //                     fontSize: 18.0,
            //                     color: Color(0xFFEF3340),
            //                   ),
            //                   textAlign: TextAlign.end,
            //                 ),
            //                 SizedBox(
            //                   width: 10.0,
            //                 ),
            //                 Icon(
            //                   Icons.arrow_forward_sharp,
            //                   color: Color(0xFFEF3340),
            //                   size: 25,
            //                 )
            //               ]),
            //         ),
            //       )
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
