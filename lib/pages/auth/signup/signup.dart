import 'package:ecommerce_app/pages/auth/login/login.dart';
import 'package:flutter/material.dart';

import '../../../core/constants/theme.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();

  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  bool isPassword= true;
  bool isPassword2= true;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(left: 40.0,right: 40,bottom: 85),
            child: Form(
              key: formKey,
              child:  Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  ///Header Of register
                  const SizedBox(
                    height: 53,
                  ),
                  //todo: put logo of app
                  const CircleAvatar(
                    radius: 50,
                    backgroundColor: ThemeColors.primary,
                  ),
                  const SizedBox(
                    height: 46,
                  ),
                  const Text("Register",
                      style: TextStyle(
                        color: Color.fromRGBO(0, 51, 102, 1),
                        fontWeight: FontWeight.w600,
                        fontSize: 35,)),
                  const SizedBox(
                    height: 13,
                  ),
                  const Text("Register to continue using the app",
                      style: TextStyle(
                        color: Color.fromRGBO(0, 51, 102, 0.6),
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      )),

                  ///inputs of login
                  const SizedBox(
                    height: 54,
                  ),
                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Email";
                      } else {
                        return null;
                      }
                    },
                    decoration:  InputDecoration(
                      fillColor: const Color.fromRGBO(243, 243, 245, 1),
                      filled: true,
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      // hintText: hintText,
                      // hintStyle: hintStyle,
                      labelText: "Email",
                      labelStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(0, 51, 102, 0.8)
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(0, 51, 102, 0.8),


                        ),

                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(0, 51, 102, 0.8),
                          
                          
                        ),
                        
                      ),
                      border: const OutlineInputBorder(),

                      prefixIcon: const Icon(
                        Icons.person_2_outlined,
                        color: Color.fromRGBO(0, 51, 102, 1),
                      ),


                    ),
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                  TextFormField(
                    controller: passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Password";
                      } else {
                        return null;
                      }
                    },
                    //todo
                    obscureText: isPassword?true:false,

                    decoration:  InputDecoration(
                      fillColor: const Color.fromRGBO(243, 243, 245, 1),
                      filled: true,
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      // hintText: hintText,
                      // hintStyle: hintStyle,
                      labelText: "Password",
                      labelStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(0, 51, 102, 0.8)
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(0, 51, 102, 0.8),


                        ),

                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(0, 51, 102, 0.8),


                        ),

                      ),
                      border: const OutlineInputBorder(),
                      prefixIcon: const Icon(Icons.lock_outline,
                        color: Color.fromRGBO(0, 51, 102, 1),
                      ),
                      //todo
                      suffixIcon:  IconButton(
                        icon: Icon(
                            isPassword ?Icons.visibility_off:
                            Icons.visibility_rounded),
                        onPressed: (){
                          setState(() {
                            isPassword=!isPassword;
                          });
                        },
                      ),



                    ),
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                  TextFormField(
                    controller: confirmPasswordController,
                    keyboardType: TextInputType.visiblePassword,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Confirm Password";
                      } else {
                        return null;
                      }
                    },
                    //todo
                    obscureText: isPassword2?true:false,

                    decoration:  InputDecoration(
                      fillColor: const Color.fromRGBO(243, 243, 245, 1),
                      filled: true,
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      // hintText: hintText,
                      // hintStyle: hintStyle,
                      labelText: "Confirm password",
                      labelStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(0, 51, 102, 0.8)
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(0, 51, 102, 0.8),


                        ),

                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(0, 51, 102, 0.8),


                        ),

                      ),
                      border: const OutlineInputBorder(),
                      prefixIcon: const Icon(Icons.lock_outline,
                        color: Color.fromRGBO(0, 51, 102, 1),
                      ),
                      //todo
                      suffixIcon:  IconButton(
                        icon: Icon(
                            isPassword2 ?Icons.visibility_off:
                            Icons.visibility_rounded),
                        onPressed: (){
                          setState(() {
                            isPassword2=!isPassword2;
                          });
                        },
                      ),



                    ),
                  ),
                  const SizedBox(
                    height: 34,
                  ),
                  Container(
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                        color:const Color.fromRGBO(0, 51, 102, 1),
                        borderRadius: BorderRadius.circular(10)
                    ),

                    child: MaterialButton(
                      onPressed: (){
                        if(formKey.currentState!.validate()){
                          ///todo:navigation to home
                        }
                      },
                      child: const Text(
                        "Registerxs",
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            color: Colors.white, fontSize: 18 ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 68,
                  ),

                  ///login with social
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(child: Divider(
                          indent: 1,
                          endIndent: 20,
                          thickness: 2,
                          color: Color.fromRGBO(0, 0, 0, 0.5)
                      )),
                      Text("or",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 0.9),
                              fontWeight: FontWeight.w500,
                              fontSize: 16
                          )),
                      Expanded(child: Divider(
                          thickness: 2,
                          indent: 20,
                          endIndent: 1,
                          color: Color.fromRGBO(0, 0, 0, 0.5)
                      ))
                    ],
                  ),
                  const SizedBox(
                    height: 42,
                  ),
                  //todo: put icons of social login
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: 25,
                        backgroundColor: ThemeColors.primary,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      CircleAvatar(
                        radius: 25,
                        backgroundColor: ThemeColors.primary,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      CircleAvatar (
                        radius: 25,
                        backgroundColor: ThemeColors.primary,
                      ),

                    ],
                  ),
                  const SizedBox(
                    height: 29,
                  ),





                  ///do not have account
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "Already have an account? ",
                        style: TextStyle(
                            fontSize: 11,
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontWeight: FontWeight.w500),
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen(),));
                          },
                          child: const Text(
                            "Login",
                            style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(0, 51, 102, 1)),
                          ))
                    ],
                  ),



                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}