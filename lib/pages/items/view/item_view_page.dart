import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../core/constants/theme.dart';

class ItemViewPage extends StatefulWidget {
  const ItemViewPage({super.key});

  @override
  State<ItemViewPage> createState() => _ItemViewPageState();
}

class _ItemViewPageState extends State<ItemViewPage>
    with TickerProviderStateMixin {
  late TabController tabviewController;

  @override
  void initState() {
    super.initState();
    tabviewController = TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(
          Icons.arrow_back,
          size: 30,
        ),
        title: Text('Universe',
            style: TextStyle(
                fontFamily: GoogleFonts.roboto.toString(),
                fontSize: 24,
                fontWeight: FontWeight.w600)),
        actions: const [
          Padding(
              padding: EdgeInsets.only(right: 18),
              child: Icon(Icons.favorite_border))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 18, right: 18),
        child: Column(
          children: [
            const SizedBox(height: 51),
            const SliderWidget(),
            const SizedBox(height: 53),
            const ItemRowWidget(),
            const SizedBox(height: 53),
            TabViewWidget(tabviewController: tabviewController),
            TabBareViewWidget(tabviewController: tabviewController),
            const SizedBox(height: 47),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const ButtonWidget(),
                Container(
                  height: 52,
                  width: 163,
                  decoration: BoxDecoration(
                      color: ThemeColors.primary,
                      borderRadius: BorderRadius.circular(60)),
                  child: TextButton(
                    onPressed: () {},
                    child: Text(
                      "ADD TO CART",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: GoogleFonts.inter.toString(),
                          fontSize: 18),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
