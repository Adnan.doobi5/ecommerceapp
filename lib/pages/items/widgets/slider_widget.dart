import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_app/core/constants/theme.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

// ignore: must_be_immutable
class SliderWidget extends StatefulWidget {
  const SliderWidget({Key? key}) : super(key: key);

  @override
  State<SliderWidget> createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  int activeIndex = 0;

  @override
  Widget build(BuildContext context) {
    Size mediaQuery = MediaQuery.of(context).size;
    return Column(
      children: [
        CarouselSlider.builder(
            options: CarouselOptions(
                height: 206,
                initialPage: 0,
                autoPlay: true,
                viewportFraction: 1.0,
                enableInfiniteScroll: false,
                scrollDirection: Axis.horizontal,
                onPageChanged: (index, reason) {
                  activeIndex = index;
                  setState(() {});
                }),
            itemCount: 4,
            itemBuilder: (context, index, realIndex) {
              return SizedBox(
                height: 206,
                width: mediaQuery.width,
                child: Image.asset(
                  "assets/images/test_image.png",
                  height: 206,
                  width: mediaQuery.width,
                  alignment: Alignment.center,
                ),
              );
            }),
        const SizedBox(height: 16),
        //Three animated smooth
        Container(
            height: 8,
            alignment: Alignment.center,
            child: AnimatedSmoothIndicator(
                activeIndex: activeIndex,
                count: 4,
                axisDirection: Axis.horizontal,
                effect: const SlideEffect(
                    paintStyle: PaintingStyle.stroke,
                    activeDotColor: ThemeColors.primary,
                    dotColor: ThemeColors.accent,
                    dotHeight: 11,
                    dotWidth: 11))),
      ],
    );
  }
}
