import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TabBareViewWidget extends StatelessWidget {
  const TabBareViewWidget({required this.tabviewController, super.key});

  final TabController tabviewController;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.only(top: 42),
        child: TabBarView(
          controller: tabviewController,
          children: [
            Text(
              "Panka Chair is a unique piece of furniture that blends elegance and comfort. Its modern design can complement various types of decor.",
              style: TextStyle(
                color: Colors.black.withOpacity(.5),
                fontSize: 16,
                fontFamily: GoogleFonts.inter.toString(),
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              "Panka Chair is a unique piece of furniture that blends elegance and comfort. Its modern design can complement various types of decor.",
              style: TextStyle(
                color: Colors.black.withOpacity(.5),
                fontSize: 16,
                fontFamily: GoogleFonts.inter.toString(),
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              "Panka Chair is a unique piece of furniture that blends elegance and comfort. Its modern design can complement various types of decor.",
              style: TextStyle(
                color: Colors.black.withOpacity(.5),
                fontSize: 16,
                fontFamily: GoogleFonts.inter.toString(),
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              "Panka Chair is a unique piece of furniture that blends elegance and comfort. Its modern design can complement various types of decor.",
              style: TextStyle(
                color: Colors.black.withOpacity(.5),
                fontSize: 16,
                fontFamily: GoogleFonts.inter.toString(),
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
