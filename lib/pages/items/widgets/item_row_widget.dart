import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../core/constants/theme.dart';

class ItemRowWidget extends StatelessWidget {
  const ItemRowWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text("Panka Chair",
            style: TextStyle(
                fontFamily: GoogleFonts.inter.toString(),
                fontWeight: FontWeight.w500,
                fontSize: 22)),
        const SizedBox(width: 8),
        const Icon(
          Icons.star_border,
          color: Color(0xFF5B79F1),
          size: 17,
        ),
        const SizedBox(width: 2),
        Text("4.5",
            style: TextStyle(
              fontFamily: GoogleFonts.inter.toString(),
              fontWeight: FontWeight.w500,
              fontSize: 14,
            )),
        const Spacer(),
        Text("\$${1000}",
            style: TextStyle(
              color: ThemeColors.primary,
              fontFamily: GoogleFonts.inter.toString(),
              fontWeight: FontWeight.w500,
              fontSize: 22,
            )),
      ],
    );
  }
}
