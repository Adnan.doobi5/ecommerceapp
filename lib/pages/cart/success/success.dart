import 'package:ecommerce_app/core/constants/theme.dart';
import 'package:flutter/material.dart';

class SuccessCheckoutPage extends StatelessWidget {
  const SuccessCheckoutPage({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: size.height,
            ),
            Positioned.fill(
                top: 230,
                right: -120,
                child: Container(
                  width: size.width * 0.8,
                  height: size.width * 0.8,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: Color(0XFF5977EF).withOpacity(0.6),
                          blurRadius: 100)
                    ],
                  ),
                )),
            Positioned(
                top: -30,
                left: -50,
                child: Container(
                  width: size.width,
                  height: size.width,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: Color(0XFFFFA07A).withOpacity(0.8),
                          blurRadius: 100)
                    ],
                  ),
                )),
            Positioned(
                bottom: -200,
                left: -200,
                child: Container(
                  width: size.width,
                  height: size.width,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: Color(0XFF003366).withOpacity(0.4),
                          blurRadius: 100)
                    ],
                  ),
                )),
            Positioned.fill(
                child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Stack(
                      clipBehavior: Clip.none,
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: 169,
                          height: 169,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.white),
                          child: Center(
                            child: Container(
                              width: 142,
                              height: 142,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                  border: Border.all(
                                      color: ThemeColors.primary, width: 1)),
                              child: Center(
                                child: Icon(
                                  Icons.check,
                                  color: ThemeColors.primary,
                                  size: 70,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                            top: 0,
                            left: -30,
                            child: circle_container(
                                height: 17,
                                color: Color(0XFFFB2D2D),
                                fill: false)),
                        Positioned(
                            left: -20,
                            bottom: 60,
                            child: circle_container(
                                height: 9,
                                color: Color(0XFFB06969),
                                fill: false)),
                        Positioned(
                            left: -20,
                            bottom: -10,
                            child: circle_container(
                                height: 15,
                                color: Color(0XFF5977EF),
                                fill: true)),
                        Positioned(
                            left: 90,
                            bottom: -40,
                            child: circle_container(
                                height: 11, color: Colors.white, fill: false)),
                        Positioned(
                            left: 120,
                            bottom: -25,
                            child: circle_container(
                                height: 18,
                                color: Color(0XFF0080FF),
                                fill: false)),
                        Positioned(
                            right: -20,
                            child: circle_container(
                                height: 12,
                                color: Color(0XFFAD00FF),
                                fill: false)),
                        Positioned(
                            top: 20,
                            right: -50,
                            child: circle_container(
                                height: 13,
                                color: Color(0XFFEF5959),
                                fill: true)),
                        Positioned(
                            top: -30,
                            right: 60,
                            child: Stack(
                              children: [
                                CustomPaint(
                                  painter: CurvedCustomPaint(
                                      color: Colors.white,
                                      startPoint: Offset(4, 15),
                                      midPoint: Offset(20, 0),
                                      lastPoint: Offset(55, 10)),
                                ),
                                CustomPaint(
                                  painter: CurvedCustomPaint(
                                      color: Color(0XFF003366).withOpacity(0.5),
                                      startPoint: Offset(0, 5),
                                      midPoint: Offset(35, 20),
                                      lastPoint: Offset(50, 0)),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    "Your Order Has Been Accepted",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: 260,
                    child: Text(
                      "your items has been placed and is on it’s way to being processed",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontFamily: 'Roboto',
                        color: Colors.white.withOpacity(0.8),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  FilledButton(
                      onPressed: () {},
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(ThemeColors.primary)),
                      child: Text(
                        "TRACK ORDER",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "back to home".toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

  Container circle_container(
      {required double height, required Color color, required bool fill}) {
    return Container(
      width: height,
      height: height,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: fill ? color : Colors.transparent,
          border: Border.all(width: 1, color: color)),
    );
  }
}

class CurvedCustomPaint extends CustomPainter {
  final Color color;
  final Offset startPoint;
  final Offset midPoint;
  final Offset lastPoint;

  CurvedCustomPaint(
      {required this.color,
      required this.startPoint,
      required this.midPoint,
      required this.lastPoint});

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;
    final path = Path();
    path.moveTo(startPoint.dx, startPoint.dy);
    path.quadraticBezierTo(
        midPoint.dx, midPoint.dy, lastPoint.dx, lastPoint.dy);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
