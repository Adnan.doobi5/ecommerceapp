import 'package:flutter/material.dart';
import 'package:test/pages/cart/checkout/checkout.dart';

class Cart extends StatefulWidget {
  Cart({super.key});

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  int number1 = 0;

  int number2 = 0;

  int number3 = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          const SizedBox(
            height: 25,
          ),
          const Row(
            children: [
              Icon(
                Icons.arrow_back,
                size: 30,
              ),
              Text("Your cart")
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            height: 450,
            child: ListView(children: [
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Expanded(
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset("assets/logo.jpg"))),
                    Expanded(
                        child: Column(
                      children: [
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            " panka chair ",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            " 1000\$",
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            InkWell(
                              child: const Icon(
                                Icons.remove_circle_outline,
                                size: 30,
                              ),
                              onTap: () {
                                setState(() {
                                  number1--;
                                });
                              },
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(number1.toString()),
                            const SizedBox(
                              width: 10,
                            ),
                            InkWell(
                              child: const Icon(Icons.add_circle_outline_sharp,
                                  size: 30),
                              onTap: () {
                                setState(() {
                                  number1++;
                                });
                              },
                            )
                          ],
                        )
                      ],
                    )),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Expanded(
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset("assets/logo.jpg"))),
                    Expanded(
                        child: Column(
                      children: [
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            " panka chair ",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            " 1000\$",
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            InkWell(
                              child: const Icon(
                                Icons.remove_circle_outline,
                                size: 30,
                              ),
                              onTap: () {
                                setState(() {
                                  number2--;
                                });
                              },
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(number2.toString()),
                            const SizedBox(
                              width: 10,
                            ),
                            InkWell(
                              child: const Icon(Icons.add_circle_outline_sharp,
                                  size: 30),
                              onTap: () {
                                setState(() {
                                  number2++;
                                });
                              },
                            )
                          ],
                        )
                      ],
                    )),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Expanded(
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset("assets/logo.jpg"))),
                    Expanded(
                        child: Column(
                      children: [
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            " panka chair ",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        const SizedBox(
                          width: double.infinity,
                          child: Text(
                            " 1000\$",
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            InkWell(
                              child: const Icon(
                                Icons.remove_circle_outline,
                                size: 30,
                              ),
                              onTap: () {
                                setState(() {
                                  number3--;
                                });
                              },
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(number3.toString()),
                            const SizedBox(
                              width: 10,
                            ),
                            InkWell(
                              child: const Icon(Icons.add_circle_outline_sharp,
                                  size: 30),
                              onTap: () {
                                setState(() {
                                  number3++;
                                });
                              },
                            )
                          ],
                        )
                      ],
                    )),
                  ],
                ),
              ),
            ]),
          ),
          Expanded(
              child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Total Amount",
                  style: TextStyle(fontSize: 20),
                ),
                const Text(
                  "\$ 300.00",
                  style: TextStyle(fontSize: 20),
                ),
                const SizedBox(
                  height: 15,
                ),
                InkWell(
                  child: Container(
                    width: 120,
                    height: 30,
                    decoration: BoxDecoration(
                        color: Colors.lightBlueAccent,
                        borderRadius: BorderRadius.circular(50)),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "CHECKOUT",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.arrow_forward,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CheckOut();
                    }));
                  },
                )
              ],
            ),
          ))
        ],
      ),
    );
  }
}
